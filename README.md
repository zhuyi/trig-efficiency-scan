# AthAnalysis framework for trigger efficiency plots from AOD
Modified from [NScanEfficiency](https://gitlab.cern.ch/zhongyuk/nscanefficiency)

## Quickstart
Compile
```
mkdir build; cd build; asetup Athena,24.0.23; cmake ../source; make -j100
```

Run
```
source build/x86_64-el9-gcc13-opt/setup.sh
source run.sh
```

Plot
```
cd script
root -l Compare.cpp
```
