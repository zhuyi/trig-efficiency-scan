#include "inc/setting.h"

std::map<TString, TString> triggers = {
	                               {"HLT_j70_j50a_j0_DJMASS1000j50dphi200x400deta_L1MJJ-500-NFF", "Run 2 VBF"},
                                     //{"HLT_j70_j50a_j0_DJMASS900j50_L1MJJ-500-NFF",                 "mjj>900GeV"},
                                     //{"HLT_j70_j50a_j0_DJMASS900j50x200deta_L1MJJ-500-NFF",         "mjj>900GeV, #Delta#eta>2"},
                                     //{"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF",  "#splitline{mjj>900GeV, #Delta#eta>2}{#Delta#phi<2.6}"},
	                               {"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF",  "loose VBF"},
				       {"HLT_j20_calratio_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj20emf24_L1MJJ-500-NFF", "loose VBF + CalRatio"},
				       {"HLT_j20_calratio_roiftf_preselj20emf24_L1J100",              "CalRatio + L1"}
				     //{"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj10emf50_L1MJJ-500-NFF", "#splitline{loose VBF}{EMF presel < 50}"},
				     //{"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj10emf70_L1MJJ-500-NFF", "#splitline{loose VBF}{EMF presel < 70}"},
                                      };

void GetPassTrig()
{
    //TString scheme = "run2_ggF/grid_run";
    //TString scheme = "new_llp_trigger/500757/rm_calratio";
    TString scheme = "new_llp_trigger/500758/backup";

    gStyle->SetPaintTextFormat("2.2f");
    TH1::SetDefaultSumw2();
    gStyle->SetOptStat(0);
    gStyle->SetEndErrorSize(0);

    TString path = "/afs/cern.ch/work/z/zhuyi/VBFDPJ/scripts/NScanEfficiency/run/" + scheme + "/";
    TString name = "myfile.root";
    auto file = new TFile(path + "/" + name, "READ");
    auto total = dynamic_cast<TH1F*>(file->Get("mjj_recon_den"));
    auto original = dynamic_cast<TH1F*>(file->Get("passTrig_recon"));

    auto hist = new TH1F("selected_passTrig_recon", "", triggers.size(), 0, triggers.size());
    hist->GetXaxis()->SetNdivisions(triggers.size(), 1, 1);

    int i = 1;
    for (int j = 0; j < original->GetNbinsX(); j++)
    {
	std::cout << original->GetXaxis()->GetLabels()->At(j)->GetName() << std::endl;
        if(triggers.find(original->GetXaxis()->GetLabels()->At(j)->GetName()) != triggers.end())
	{
	    hist->SetBinContent(i, original->GetBinContent(j + 1));
	    std::cout << original->GetBinContent(j + 1) << std::endl;
            hist->GetXaxis()->ChangeLabel(i, 0., -1, 22, -1, -1, triggers.at(original->GetXaxis()->GetLabels()->At(j)->GetName()));
	    i++;
	}
    }

    hist->GetYaxis()->SetTitle("Efficiency");
    hist->GetXaxis()->ChangeLabel(hist->GetNbinsX(), -1, 0., -1, -1, -1, "");
    hist->GetXaxis()->CenterLabels();
    hist->SetLabelSize(original->GetLabelSize());
    hist->SetLabelOffset(0.018);
    hist->Scale(1./(total->GetSumOfWeights() + total->GetBinContent(total->GetNbinsX() + 1)));
    hist->SetMaximum(1.);
    hist->SetMinimum(0.);
    hist->SetMarkerSize(2);

    auto c = new TCanvas("c", "", 800., 600.);
    hist->Draw("hist text0");

    const int n_bins = hist->GetNbinsX();
    std::vector<TH1F*> hists;
    for(int i = 0; i < n_bins; i++)
    {
        hists.push_back(dynamic_cast<TH1F*>(hist->Clone()));

        for(int j = 0; j < n_bins; j++)
        {
            if(j != i)
                hists.back()->SetBinContent(j + 1, 0.);
        }

        hists.back()->SetLineColor(colors.at(i));
        hists.back()->SetFillColor(colors.at(i));
        hists.back()->Draw("hist same");
    }

    TString scheme_name = scheme;
    scheme_name.ReplaceAll("/", "_");
    TString output = "pass_trig_" + scheme_name;
    c->SaveAs("fig/" + output + ".png");
}
