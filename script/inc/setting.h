#ifndef SETTING_H
#define SETTING_H

const std::map<TString, TString> labels = {{"yd_pt", "dy p^{T} [GeV]"},
                                           {"dR", "dy #DeltaR"},
                                           {"mjj", "truth mjj [GeV]"},
                                           {"mjj_truth", "truth mjj [GeV]"},
                                           {"mjj_recon", "recon mjj [GeV]"}};
const std::vector<int> markers = {
                                  //20, // HLT_j70_j50a_j0_DJMASS1000j50dphi200x400deta_L1MJJ-500-NFF
                                  //21, // HLT_j70_j50a_j0_DJMASS900j50_L1MJJ-500-NFF
                                  //22, // HLT_j70_j50a_j0_DJMASS900j50x200deta_L1MJJ-500-NFF
                                  //23, // HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF
                                  //33, // HLT_j20_calratio_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj20emf24_L1MJJ-500-NFF
                                  //27, // HLT_j20_calratio_roiftf_preselj20emf24_L1MJJ-500-NFF
                                  //27, // HLT_j20_calratio_roiftf_preselj20emf70_L1MJJ-500-NFF
                                  //34, // HLT_j10_calratio_j70_j50a_j0_DJMASS900j50_roiftf_preselj10emf24_L1MJJ-500-NFF
                                  //34,

                                  20,
                                  21,
                                  22,
                                  23,
                                 };
const std::vector<int> colors  = {
                                  //TColor::GetColor("#000000"), // HLT_j70_j50a_j0_DJMASS1000j50dphi200x400deta_L1MJJ-500-NFF
                                  //TColor::GetColor("#F96836"), // HLT_j70_j50a_j0_DJMASS900j50_L1MJJ-500-NFF
                                  //TColor::GetColor("#950603"), // HLT_j70_j50a_j0_DJMASS900j50x200deta_L1MJJ-500-NFF
                                  //TColor::GetColor("#4563CB"), // HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF
                                  //TColor::GetColor("#3C0098"), // HLT_j20_calratio_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj20emf24_L1MJJ-500-NFF
                                  //TColor::GetColor("#660066"), // HLT_j20_calratio_roiftf_preselj20emf24_L1MJJ-500-NFF
                                  //TColor::GetColor("#ff3399"), // HLT_j20_calratio_roiftf_preselj20emf70_L1MJJ-500-NFF
                                  //TColor::GetColor("#073007"),
                                  //TColor::GetColor("#006600"), // HLT_j10_calratio_j70_j50a_j0_DJMASS900j50_roiftf_preselj10emf24_L1MJJ-500-NFF
                                  TColor::GetColor("#000000"),
                                  TColor::GetColor("#F96836"),
                                  TColor::GetColor("#950603"),
                                  TColor::GetColor("#4563CB"),
                                 };

#endif // SETTING_H
