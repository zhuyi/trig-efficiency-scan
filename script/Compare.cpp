#include "inc/setting.h"

std::vector<TString> triggers = {//"HLT_mu20_msonly_iloosems_mu6noL1_msonly_nscan10_L110DR-MU14FCH-MU5VF",
                                 //"HLT_mu20_msonly_iloosems_mu6noL1_msonly_nscan20_L110DR-MU14FCH-MU5VF",
                                 //"HLT_mu20_msonly_iloosems_mu6noL1_msonly_nscan30_L110DR-MU14FCH-MU5VF",
                                 //"HLT_mu20_msonly_iloosems_mu6noL1_msonly_nscan40_L110DR-MU14FCH-MU5VF"
                                 //"HLT_j70_j50_0eta490_invm1100j70_dphi20_deta40_L1MJJ-500-NFF",
                                 //"HLT_j70_j50_0eta490_invm1100j70_dphi20_deta45_L1MJJ-500-NFF",
                                 //"HLT_j70_j50_0eta490_invm1100j70_dphi24_deta40_L1MJJ-500-NFF",
                                 //"HLT_j70_j50_0eta490_invm900j50_dphi26_L1MJJ-500-NFF"

                                 //"HLT_j70_j50a_j0_DJMASS1000j50dphi200x400deta_L1MJJ-500-NFF",
                                 //"HLT_j70_j50a_j0_DJMASS900j50_L1MJJ-500-NFF",
                                 //"HLT_j70_j50a_j0_DJMASS900j50x200deta_L1MJJ-500-NFF",
                                 //"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF",
                                 //"HLT_j20_calratio_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj20emf24_L1MJJ-500-NFF",
                                 //"HLT_j20_calratio_roiftf_preselj20emf24_L1J100",
                                 //"HLT_j20_calratio_roiftf_preselj20emf24_L1MJJ-500-NFF",
                                 //"HLT_j20_calratio_roiftf_preselj20emf70_L1MJJ-500-NFF",
                                 //"HLT_j10_calratio_j70_j50a_j0_DJMASS900j50_roiftf_preselj10emf24_L1MJJ-500-NFF",
                                 //"HLT_j10_calratio_j70_j50a_j0_DJMASS900j50_roiftf_preselj10emf70_L1MJJ-500-NFF",
                                 //"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj10emf70_L1MJJ-500-NFF",
                                 //"HLT_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj10emf50_L1MJJ-500-NFF",

                                 "HLT_mu6_msonly_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF",
                                 "HLT_mu6noL1_msonly_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF",
                                 "HLT_2mu6noL1_msonly_nscan_j70_j50a_j0_DJMASS900j50dphi260x200deta_L1MJJ-500-NFF",
                                 //"HLT_mu6_msonly_j50_j30a_j0_DJMASS900j30dphi260x200deta_L1MJJ-500-NFF",
                                 //"HLT_2mu6noL1_msonly_nscan_j50_j30a_j0_DJMASS900j30dphi260x200deta_L1MJJ-500-NFF",
                                 //"HLT_j20_calratiovar_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj20emf72_L1MJJ-500-NFF",
                                 //"HLT_j20_calratiovar135_j70_j50a_j0_DJMASS900j50dphi260x200deta_roiftf_preselj20emf48_L1MJJ-500-NFF",
                                 //"HLT_j20_CLEANllp_momemfrac072_calratiovar59_roiftf_preselj20emf72_L1MJJ-500-NFF",
                                 //"HLT_j20_CLEANllp_momemfrac048_calratiovar135_roiftf_preselj20emf48_L1MJJ-500-NFF"
                                };

void Compare()
{
    TString var = "mjj_recon";
    TString channel = "muonic";

    //TString scheme = "new_VBF_threshold/grid_run";
    //TString scheme = "new_llp_trigger/521260";
    //TString scheme = "new_llp_trigger/500758/backup";
    //TString scheme = "new_llp_trigger/500757/rm_calratio";
    //TString scheme = "new_llp_trigger/521260/rm_calratio";
    TString scheme = "";

    TH1::SetDefaultSumw2();
    gStyle->SetOptStat(0);
    gStyle->SetEndErrorSize(0);

    TString path = "/afs/cern.ch/work/z/zhuyi/VBFDPJ/scripts/NScanEfficiency/run/" + scheme + "/";
    TString name = "myfile.root";
    auto file = new TFile(path + "/" + name, "READ");

    auto ref = dynamic_cast<TH1F*>(file->Get(var + "_" + channel + "_den"));
    std::cout << var + "_" + channel + "_den" << std::endl;
    std::cout << ref << std::endl;
    const int    n_bin = 12;
    const double lower = 800;
    const double upper = 2000;
    //int rebinning = ref->GetNbinsX()/(ref->GetXaxis()->GetBinUpEdge(ref->GetNbinsX())/upper)/n_bin;
    //std::cout << ref->GetNbinsX() << std::endl;
    //std::cout << rebinning << std::endl;
    //ref->Rebin(rebinning);

    std::vector<TGraphAsymmErrors*> effs;
    for(auto trigger = triggers.begin(); trigger != triggers.end(); ++trigger)
    {
        TString hist_name = var + "_" + *trigger;
        std::cout << hist_name << std::endl;

        const int i = trigger - triggers.begin();

        auto triggered = dynamic_cast<TH1F*>(file->Get(hist_name));
        std::cout << triggered << std::endl;
        if(!triggered)
        {
            std::cerr << "No hist named " << hist_name << " found" << std::endl;
            exit(-1);
        }
        triggered->SetBinContent(0, 0.);
        //triggered->Rebin(rebinning);

        auto teff = new TEfficiency(*triggered, *ref);
        auto temp = teff->CreateGraph();

        std::vector<double> xs;
        std::vector<double> ys;
        std::vector<double> xehs;
        std::vector<double> xels;
        std::vector<double> yehs;
        std::vector<double> yels;
        for(int j = 1; j <= n_bin; j++)
        {
            bool if_overflow = std::isnormal(teff->GetTotalHistogram()->GetBinContent(j));

//            auto x = (j - 0.5)*(upper - lower)/n_bin + lower;
            auto x = teff->GetTotalHistogram()->GetBinCenter(j);
            auto y = if_overflow ? temp->GetY()[j - 2] : -1.;
            auto xeh = if_overflow ? temp->GetEXhigh()[j - 2] : 0.;
            auto xel = if_overflow ? temp->GetEXlow()[j - 2] : 0.;
            auto yeh = if_overflow ? temp->GetEYhigh()[j - 2] : 0.;
            auto yel = if_overflow ? temp->GetEYlow()[j - 2] : 0.;

            std::cout << x << "\t" << y << "\t" << temp->GetY()[j - 2] << std::endl;

            xs.push_back(x);
            ys.push_back(y);
            xehs.push_back(xeh);
            xels.push_back(xel);
            yehs.push_back(yeh);
            yels.push_back(yel);
        }
        std::cout << std::endl;

        effs.push_back(new TGraphAsymmErrors(n_bin, &xs.at(0), &ys.at(0), &xels.at(0), &xehs.at(0), &yels.at(0), &yehs.at(0)));
        effs.back()->SetName(*trigger);
        effs.back()->SetTitle(" ");
        effs.back()->SetMarkerStyle(markers.at(i));
        effs.back()->SetMarkerColor(colors.at(i));
        effs.back()->SetLineColor(colors.at(i));
    }

    auto c = new TCanvas("c", "", 800, 600);

    effs.front()->Draw("AP");
    effs.front()->GetXaxis()->SetNdivisions(10, 10, 0);
    effs.front()->GetXaxis()->SetRangeUser(0., upper);
    effs.front()->GetYaxis()->SetRangeUser(0., 1.3);
    effs.front()->GetXaxis()->SetTitle(labels.at(var));
    effs.front()->GetYaxis()->SetTitle("Efficiency");

    for(const auto eff : effs) eff->Draw("P same");

    TLegend *legend = new TLegend(0.12, 0.73, 0.50, 0.85);
    for(const auto &eff : effs) legend->AddEntry(eff, eff->GetName(), "LP");
    legend->SetTextSize(0.025);
    legend->SetBorderSize(0);
    legend->Draw();

    TString scheme_name = scheme;
    scheme_name.ReplaceAll("/", "_");
    TString output = "trigger_" + scheme_name + "_" + var;
    //c->SaveAs("fig/" + output + "_mr_calratio.png");
}
