import os
import sys
from glob import glob


#Skeleton joboption for a simple analysis job

#---- Minimal job options -----
jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
#jps.AthenaCommonFlags.TreeName = "MyTree"                    #when using TreeAccess, must specify the input tree name

jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]  #register output files like this. MYSTREAM is used in the code
athAlgSeq += CfgMgr.MyPackageAlg()                               #adds an instance of your alg to the main alg sequence

#---- Options you could specify on command line -----
#jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
#jps.AthenaCommonFlagsI.FilesInput = ["/eos/user/d/dljmcwh/storage/sampleT/mc16_13TeV.311949.MGPy8EG_A14NN23LO_FRVZ_2zd_mH125_mzd400.recon.AOD.e7459_e5984_s3126_d1663_r12711/AOD.25931589._000009.pool.root.1"]
#jps.AthenaCommonFlags.FilesInput = glob("/eos/user/d/dljmcwh/storage/mc20_13TeV.311957.MGPy8EG_A14NN23LO_FRVZ_2zd_mH800_mzd900.merge.AOD.e7459_e5984_s3126_d1722_r13135_r13147/*root*")
#jps.AthenaCommonFlags.FilesInput = glob("/eos/user/d/dljmcwh/storage/mc20_13TeV.311949.MGPy8EG_A14NN23LO_FRVZ_2zd_mH125_mzd400.merge.AOD.e7459_e5984_s3126_d1722_r13135_r13147/*root*")
#jps.AthenaCommonFlags.FilesInput = ["/eos/user/d/dljmcwh/storage/sampleT/mc16_13TeV.311949.MGPy8EG_A14NN23LO_FRVZ_2zd_mH125_mzd400.recon.AOD.e7459_e5984_s3126_d1639_r12597/AOD.25462174._000001.pool.root.1"]
#jps.AthenaCommonFlags.FilesInput = ["/eos/user/d/dljmcwh/storage/sampleT/mc16_13TeV.311949.MGPy8EG_A14NN23LO_FRVZ_2zd_mH125_mzd400.deriv.DAOD_PHYS.e7459_s3126_r12597_p4505/DAOD_PHYS.25606595._000001.pool.root.1"]

# if (os.path.isdir(jps.AthenaCommonFlags.inputDir)):
#     basedir=jps.AthenaCommonFlags.FilesInput
#     inputFiles = [os.path.join(basedir,x) for x in os.listdir(basedir) if ".root" in x]
#     jps.AthenaCommonFlags.FilesInput = inputFiles

include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above

