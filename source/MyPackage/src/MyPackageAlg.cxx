// MyPackage includes
#include "MyPackageAlg.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODJet/JetContainer.h"

//#include <TLorentzVector.h>
#include <Math/Vector4D.h>
#include "TEfficiency.h"
#include <algorithm>
#include <tuple>

template <class T>
double getMjj(const T* p1, const T* p2)
{
    ROOT::Math::PtEtaPhiEVector leading_jet(p1->pt()*0.001,
                                            p1->eta(),
                                            p1->phi(),
                                            p1->e()*0.001);
    ROOT::Math::PtEtaPhiEVector subleading_jet(p2->pt()*0.001,
                                               p2->eta(),
                                               p2->phi(),
                                               p2->e()*0.001);
    return (leading_jet + subleading_jet).M();
}

MyPackageAlg::MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){
    //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
}


MyPackageAlg::~MyPackageAlg() {}


StatusCode MyPackageAlg::initialize() {
    ATH_MSG_INFO ("Initializing " << name() << "...");
  
    m_passTrig_recon = nullptr;  
  
    m_mjj_recon_hadron_den = new TH1F("mjj_recon_hadron_den","mjj_recon_den",12,800,2000);
    CHECK( histSvc()->regHist("/MYSTREAM/mjj_recon_hadron_den", m_mjj_recon_hadron_den) );

    m_yd_pt_hadron_den = new TH1F("yd_pt_hadron_den","yd_pt_den",16,0,200);
    CHECK( histSvc()->regHist("/MYSTREAM/yd_pt_hadron_den", m_yd_pt_hadron_den) );

    m_mjj_recon_muonic_den = new TH1F("mjj_recon_muonic_den","mjj_recon_den",12,800,2000);
    CHECK( histSvc()->regHist("/MYSTREAM/mjj_recon_muonic_den", m_mjj_recon_muonic_den) );

    m_yd_pt_muonic_den = new TH1F("yd_pt_muonic_den","yd_pt_den",16,0,200);
    CHECK( histSvc()->regHist("/MYSTREAM/yd_pt_muonic_den", m_yd_pt_muonic_den) );
 
    m_jet_emf = new TH1F("jet_emf_min","jet_emf_min",50,0,1);
    CHECK( histSvc()->regHist("/MYSTREAM/jet_emf_min", m_jet_emf) );

    m_jet_calratio = new TH1F("jet_calratio_min","jet_calratio_min",50,-5,5);
    CHECK( histSvc()->regHist("/MYSTREAM/jet_calratio_min", m_jet_calratio) );

    m_output_tree = new TTree("trig_decision", "trigger decision tree");
    CHECK ( histSvc()->regTree("/MYSTREAM/trig_decision", m_output_tree) );

    m_tdt.setTypeAndName("Trig::TrigDecisionTool/TrigDecisionTool");
    CHECK( m_tdt.initialize() );
  
    return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::finalize() {
    ATH_MSG_INFO ("Finalizing " << name() << "...");
    ATH_MSG_INFO ("Total events processed: "<< m_eventCounter);
    return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::execute() {  
    ATH_MSG_DEBUG ("Executing " << name() << "...");
    setFilterPassed(false); //optional: start with algorithm not passed
    m_eventCounter++;
    if (m_eventCounter%200 == 0) std::cout << m_eventCounter << std::endl;

//....................................................................................................
// Truth
//....................................................................................................
    using TruthJet = Node<xAOD::TruthParticle>;

    decay_type channel = dUnkown;

    std::vector<TruthJet> darky_jets;
    const xAOD::EventInfo* ei = 0;
    CHECK( evtStore()->retrieve( ei , "EventInfo" ) ); //retrieves the event info
   
    const xAOD::TruthEventContainer *truthEvents = nullptr;
    CHECK( evtStore()->retrieve(truthEvents, "TruthEvents"));
//    std::cout << m_eventCounter << ":\t" << truthEvents->size() << std::endl;

    unsigned short n_muonic_decay = 0;
    unsigned short n_hadron_decay = 0;
    for(auto truthEv : *truthEvents) {
        unsigned int nPart = truthEv->nTruthParticles();
  
        for(unsigned int iPart=0; iPart<nPart; ++iPart){
            const xAOD::TruthParticle *particle = truthEv->truthParticle(iPart);

	    if(particle == nullptr) continue;

	    if(particle->pdgId() != 3000001) continue;

	    if     (particle->nChildren() == 2 && std::abs(particle->child(0)->pdgId()) == 13)
        {
	        n_muonic_decay++;
		    darky_jets.emplace_back(TruthJet{particle->pt()*0.001, particle});
        }
	    else if(particle->nChildren() == 2 && (std::abs(particle->child(0)->pdgId()) == 11 || std::abs(particle->child(0)->pdgId()) < 7))
        {
		    n_hadron_decay++;
		    darky_jets.emplace_back(TruthJet{particle->pt()*0.001, particle});
        }

        }//for iPart
    }

    if(darky_jets.size() < 1) 
    {
        setFilterPassed(true);
	    return StatusCode::SUCCESS;
    }

    if(n_muonic_decay >= 2)      channel = dMuonic;
    else if(n_hadron_decay >= 1) channel = dHadron;
    else
    {
        setFilterPassed(true);
        return StatusCode::SUCCESS;
    }

    std::max_element(darky_jets.begin(), darky_jets.end(), [](const TruthJet &particle1, const TruthJet &particle2)
                                                           { return particle1.pt > particle2.pt; }
                    );
    auto yd_pt_lead = darky_jets.at(0).pt;

//....................................................................................................
// Recon
//....................................................................................................
    using ReconJet = Node<xAOD::Jet>;

    std::vector<ReconJet> recon_jets;
    const xAOD::JetContainer *jets = nullptr;
    CHECK( evtStore()->retrieve(jets, "HLT_AntiKt4EMTopoJets_nojcalib"));

    float min_emf = 10.;
    for(auto jet : *jets)
    {
        recon_jets.emplace_back(ReconJet{jet->pt()*0.001, jet});
	    if(channel == dHadron && jet->pt()*0.001 > 10. && jet->getAttribute<float>(xAOD::JetAttribute::EMFrac) < min_emf)
            min_emf = jet->getAttribute<float>(xAOD::JetAttribute::EMFrac);
    }

    if(recon_jets.size() < 2)
    {
        setFilterPassed(true);
        return StatusCode::SUCCESS;
    }

    auto mjj_recon = 0.;
    std::partial_sort(recon_jets.begin(), recon_jets.begin() + 2, recon_jets.end(), [](const ReconJet &particle1, const ReconJet &particle2)
                                                                                    { return particle1.pt > particle2.pt; }
                     );
    mjj_recon = getMjj(recon_jets.at(0).particle, recon_jets.at(1).particle);

    bool if_presel_hadron = channel == dHadron && recon_jets.at(0).pt > 70 && recon_jets.at(1).pt > 50 && mjj_recon > 900.;
    bool if_presel_muonic = channel == dMuonic && recon_jets.at(0).pt > 70 && recon_jets.at(1).pt > 50 && mjj_recon > 900.;

//....................................................................................................
// Init hists
//....................................................................................................
    auto allChains = m_tdt->getChainGroup("HLT_.*");
        
    if (!m_passTrig_recon){
        m_passTrig_recon = new TH1F("passTrig_recon","passTrig_recon",allChains->getListOfTriggers().size(),0,allChains->getListOfTriggers().size());
        CHECK( histSvc()->regHist("/MYSTREAM/passTrig_recon", m_passTrig_recon) ); //registers histogram to output stream
        for ( int i=0; i<(int)allChains->getListOfTriggers().size(); ++i){
            ATH_MSG_INFO("Found HLT chain " << allChains->getListOfTriggers()[i]);
            m_passTrig_recon->GetXaxis()->SetBinLabel(i+1,allChains->getListOfTriggers()[i].c_str());
        }
    }
  
    if (m_numerators.size()==0){
        TH1F* currentHist = nullptr;

        // All mjj_recon
        for ( int i=0; i<(int)allChains->getListOfTriggers().size(); ++i){
            std::string chainName = "mjj_recon_"+allChains->getListOfTriggers()[i];
            currentHist = new TH1F(chainName.c_str(), "", 12,800,2000);
            CHECK( histSvc()->regHist(("/MYSTREAM/"+chainName).c_str(), currentHist) );
            m_numerators.push_back(currentHist);
        }

        // All Pt
        for ( int i=0; i<(int)allChains->getListOfTriggers().size(); ++i){
            std::string chainName = "yd_pt_"+allChains->getListOfTriggers()[i];
            currentHist = new TH1F(chainName.c_str(), "", 16,0,200);
            CHECK( histSvc()->regHist(("/MYSTREAM/"+chainName).c_str(), currentHist) );
            m_numerators.push_back(currentHist);
        }
    }

//....................................................................................................
//Fill hists
//....................................................................................................

    auto DecideTrigChannel = [&](std::string trig_name) -> decay_type
                             {
                                 if(trig_name.find("msonly")   != std::string::npos) return dMuonic;
                                 if(trig_name.find("calratio") != std::string::npos) return dHadron;
                                 return dGeneric;
                             };

    auto DecideFillHisto = [&](std::string trig_name) -> bool
                           {
                               return (DecideTrigChannel(trig_name) == dMuonic && if_presel_muonic) ||
                                      (DecideTrigChannel(trig_name) == dHadron && if_presel_hadron) ||
                                      (DecideTrigChannel(trig_name) == dGeneric);
                           };


    int i=-1;
    for(auto& trig : allChains->getListOfTriggers()){
        i++;
        if ( m_tdt->isPassed(trig) && DecideFillHisto(trig)) {
             m_passTrig_recon->Fill(i);
        }
    }

    if(if_presel_hadron) m_jet_calratio->Fill(log(1./min_emf - 1.));
    if(if_presel_hadron) m_jet_emf->Fill(min_emf);

    if(if_presel_hadron) m_mjj_recon_hadron_den->Fill(mjj_recon);
    if(if_presel_hadron) m_yd_pt_hadron_den->Fill(yd_pt_lead);
    if(if_presel_muonic) m_mjj_recon_muonic_den->Fill(mjj_recon);
    if(if_presel_muonic) m_yd_pt_muonic_den->Fill(yd_pt_lead);

    int j=-1;
    // For mjj_recon
    for ( auto trig : allChains->getListOfTriggers()){
        j++;
        if ( m_tdt->isPassed(trig) && DecideFillHisto(trig) )
            m_numerators[j]->Fill(mjj_recon);
    }
    for ( auto trig : allChains->getListOfTriggers()){
        j++;
        if ( m_tdt->isPassed(trig) &&  DecideFillHisto(trig) )
            m_numerators[j]->Fill(yd_pt_lead);
    }

    setFilterPassed(true); //if got here, assume that means algorithm passed
    return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::beginInputFile() { 
    return StatusCode::SUCCESS;
}


