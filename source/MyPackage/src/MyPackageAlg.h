#ifndef MYPACKAGE_MYPACKAGEALG_H
#define MYPACKAGE_MYPACKAGEALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "AsgTools/AnaToolHandle.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "TEfficiency.h"
//Example ROOT Includes
//#include "TTree.h"
//#include "TH1D.h"
#include <vector>

template <class T>
struct Node
{
    double pt;
    const T *particle{nullptr};
};

class MyPackageAlg: public ::AthAnalysisAlgorithm { 
public: 
    MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~MyPackageAlg(); 
  
    ///uncomment and implement methods as required
    int m_eventCounter=0;
                                          //IS EXECUTED:
    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
    //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
    virtual StatusCode  execute();        //per event
    //virtual StatusCode  endInputFile();   //end of each input file
    //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
    virtual StatusCode  finalize();       //once, after all events processed
    
  
    ///Other useful methods provided by base class are:
    ///evtStore()        : ServiceHandle to main event data storegate
    ///inputMetaStore()  : ServiceHandle to input metadata storegate
    ///outputMetaStore() : ServiceHandle to output metadata storegate
    ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
    ///currentFile()     : TFile* to the currently open input file
    ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp
    asg::AnaToolHandle<Trig::TrigDecisionTool> m_tdt;

private: 
    //Example algorithm property, see constructor for declaration:
    //int m_nProperty = 0;

    //Example histogram, see initialize method for registration to output histSvc
    TH1F* m_passTrig_recon = nullptr;
    std::vector<TH1F*> m_numerators;
    TH1F* m_mjj_recon_hadron_den = nullptr;
    TH1F* m_yd_pt_hadron_den = nullptr;
    TH1F* m_mjj_recon_muonic_den = nullptr;
    TH1F* m_yd_pt_muonic_den = nullptr;

    TH1F* m_jet_emf = nullptr;
    TH1F* m_jet_calratio = nullptr;

    TTree* m_output_tree = nullptr;

    enum decay_type {dUnkown, dMuonic, dHadron, dGeneric};
}; 

#endif //> !MYPACKAGE_MYPACKAGEALG_H
