#inputFile=`ls -1 /afs/cern.ch/work/z/zhuyi/VBFDPJ/scripts/NScanEfficiency/input/NSCAN0512/conedR/* | tr "\n" ","`
#inputFile=`ls -1 /afs/cern.ch/work/z/zhuyi/VBFDPJ/scripts/NScanEfficiency/input/NSCAN0512/coneCheck2/* | tr "\n" ","`

#inputFile=`ls -1 /eos/user/y/yygao/DarkPhoton/AOD/mc16_13TeV/*500758*10726*/* | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/mc16_13TeV.521260.MGPy8EG_FRVZ_2zd_VBF_mzd2000_MJJ750.merge.AOD.*10726*/* | tr "\n" ","`

#inputFile=`ls -1 /afs/cern.ch/work/z/zhuyi/VBFDPJ/scripts/athena-24.0/run/00001/AOD.pool.00001.root.1 | tr "\n" ","`
#inputFile=`ls -1 /afs/cern.ch/work/z/zhuyi/VBFDPJ/scripts/athena-24.0/run/00003/AOD.pool.00003.root.1 | tr "\n" ","`

#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500758_mzd400/AOD/backup/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500758_mzd400/AOD/no_50_70/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500757_mzd100/AOD/no_50_70/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/521260_mzd2000/AOD/no_50_70/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/521260_mzd2000/AOD/j10/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500757_mzd100/AOD/j10/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500758_mzd400/AOD/j10/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500757_mzd100/AOD/rm_calratio/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500758_mzd400/AOD/rm_calratio/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/521260_mzd2000/AOD/rm_calratio/*.root | tr "\n" ","`
#inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500757_mzd100/AOD/*.root | tr "\n" ","`
inputFile=`ls -1 /eos/home-z/zhuyi/VBFDPJ/sample/trigger/500758_mzd400/AOD/mr_28412_scan/*.root | tr "\n" ","`

mkdir -vp run
#rm -rf run/*
cd run
athena ../source/MyPackage/share/MyPackageAlgJobOptions.py --filesInput ${inputFile}
cd ..
